       .data 0x10010000 

var1:  .word 0x10         # var1 is a word (32 bit) with the initial value 0x10. 

                       
var2:  .word 0x20         # var2 is a word (32 bit) with the initial value 0x20.
 

var3:  .word 0x30         # var3 is a word (32 bit) with the initial value 0x30. 

                       
var4:  .word 0x40         # var4 is a word (32 bit) with the initial value 0x40.


first: .byte 'Y'          #first is a byte (8 bit)  with the initial value'Y'


last:  .byte 'C'          #last is a byte (8 bit)  with the initial value'C'


       .text 
       .globl main 

main:  addu $s0, $ra, $0  # save $31 in $16 

       lui $1, 4097          
       lw $8, 0($1)
       lui $1, 4097
       lw $9, 4($1)
       lui $1, 4097 
       lw $10, 8($1)
       lui $1, 4097  
       lw $11, 12($1)
       lui $1, 4097
       lb $12, 16($1)  
       lui $1, 4097
       lb $13, 17($1)
       lui $1, 4097
       sw $11, 0($1)
       lui $1, 4097 
       sw $10, 4($1)
       lui $1, 4097
       sw $9, 8($1) 
       lui $1, 4097 
       sw $8, 12($1)

       addu $ra, $0, $s0   # return address back in $31 
       
       jr $ra              # return from main

